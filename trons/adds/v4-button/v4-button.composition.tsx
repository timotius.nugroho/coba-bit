import React from 'react';
import { V4Theme } from '@arunaseafood/trons.adds.v4-theme';
import { V4Button } from './v4-button';

export const BasicV4Button = () => {
  return (
    <>
      <V4Theme>
        <V4Button variant="contained" color="primary">
          hello world!
        </V4Button>
      </V4Theme>
    </>
  );
};
