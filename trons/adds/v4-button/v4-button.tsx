import React, { ReactNode } from 'react';
import Button from '@material-ui/core/Button';
import { ButtonProps } from '@material-ui/core/Button';

export type V4ButtonProps = ButtonProps & {
  /**
   * a node to be rendered in the special component.
   */
  children?: ReactNode;
};

export function V4Button({ children, className, ...rest }: V4ButtonProps) {
  return (
    <Button className={className} {...rest}>
      {children}
    </Button>
  );
}
