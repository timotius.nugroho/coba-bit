import React from 'react';
import { render } from '@testing-library/react';
import { BasicV4Button } from './v4-button.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicV4Button />);
  const rendered = getByText('hello world!');
  expect(rendered).toBeTruthy();
});
