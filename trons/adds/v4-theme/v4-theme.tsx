import React, { ReactNode } from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import theme from './theme';

export type V4ThemeProps = {
  /**
   * a node to be rendered in the special component.
   */
  children?: ReactNode;
};

export function V4Theme({ children }: V4ThemeProps) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
}
