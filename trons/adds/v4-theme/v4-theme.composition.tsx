import React from 'react';
import { V4Theme } from './v4-theme';
import { Button } from '@material-ui/core';

export const BasicV4Theme = () => {
  return (
    <V4Theme>
      <Button variant="contained" color="primary">
        OK v4 theme
      </Button>
    </V4Theme>
  );
};
