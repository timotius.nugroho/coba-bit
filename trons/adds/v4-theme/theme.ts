import { createTheme } from '@material-ui/core/styles';
// @ts-ignore
import ArunaSansText from './assets/ArunaSansText-Regular.ttf';

const theme = createTheme({
  typography: {
    fontFamily: 'ArunaSansText, Arial',
  },
  palette: {
    primary: {
      main: '#32a846',
    },
  },
  overrides: {
    MuiButton: {
      root: {
        textTransform: 'none',
      },
      containedPrimary: {
        padding: '15px 30px',
      },
    },
    MuiCssBaseline: {
      '@global': {
        '@font-face': [
          {
            fontFamily: 'ArunaSansText',
            fontStyle: 'normal',
            fontWeight: 400,
            src: `
              local('ArunaSansText'),
              local('ArunaSansText-Regular'), 
              url(${ArunaSansText}) format('truetype')
              `,
          },
        ],
      },
    },
  },
});

export default theme;
