import React from 'react';
import { render } from '@testing-library/react';
import { BasicV4Theme } from './v4-theme.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicV4Theme />);
  const rendered = getByText('OK v4 theme');
  expect(rendered).toBeTruthy();
});
