import React, { ReactNode } from 'react';
import { Skeleton as MuiSkeleton } from '@mui/material';
import Stack from '@mui/material/Stack';
import moment from 'moment';

export type SkeletonProps = {
  /**
   * a node to be rendered in the special component.
   */
  children?: ReactNode;
};

export function Skeleton({ children }: SkeletonProps) {
  return (
    <div>
      {children}
      {moment('20111031', 'YYYYMMDD').fromNow()}
      <Stack spacing={1}>
        {/* For variant="text", adjust the height via font-size */}
        <MuiSkeleton variant="text" sx={{ fontSize: '1rem' }} />

        {/* For other variants, adjust the size with `width` and `height` */}
        <MuiSkeleton variant="circular" width={40} height={40} />
        <MuiSkeleton variant="rectangular" width={210} height={60} />
        <MuiSkeleton variant="rounded" width={210} height={60} />
      </Stack>
    </div>
  );
}
