import React from 'react';
import { Skeleton } from './skeleton';
import { NewTheme } from '@arunaseafood/trons.main.new-theme';

export const BasicSkeleton = () => {
  return (
    <NewTheme>
      <Skeleton>hello world!</Skeleton>
    </NewTheme>
  );
};
