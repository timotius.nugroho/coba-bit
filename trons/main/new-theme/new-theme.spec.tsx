import React from 'react';
import { render } from '@testing-library/react';
import { BasicNewTheme } from './new-theme.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicNewTheme />);
  const rendered = getByText('Regular theme primary');
  expect(rendered).toBeTruthy();
});
