export { NewTheme } from './new-theme';
export type { NewThemeProps, ThemeSchema } from './new-theme';
