import React from 'react';
import {
  ThemeProvider as MuiThemeProvider,
  createTheme as muiCreateTheme,
  Theme as MuiTheme,
} from '@mui/material';
import { defaultTheme } from './theme';

export type ThemeSchema = {} & MuiTheme;

export type NewThemeProps = {
  theme?: ThemeSchema;
  children: any;
};

export const createTheme = (themeValues): MuiTheme => {
  return muiCreateTheme(themeValues);
};

export function NewTheme({
  /* the default theme will be created in the next step */
  theme = createTheme(defaultTheme),
  children,
}: NewThemeProps) {
  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}
