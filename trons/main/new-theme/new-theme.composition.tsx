import React from 'react';
import Button from '@mui/material/Button';
import { NewTheme } from './new-theme';

export const BasicNewTheme = () => {
  return (
    <NewTheme>
      <Button variant="contained">Regular theme primary</Button>
      <Button variant="contained" color="secondary">
        Regular theme secondary
      </Button>
    </NewTheme>
  );
};
