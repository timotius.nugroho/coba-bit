import React from 'react';
import { NewButton } from './new-button';

export const MuiButtonVariants = () => {
  const variants = ['contained', 'outlined', 'text'] as const;

  return (
    <div className="vertical">
      {variants.map((v, index) => (
        <>
          <NewButton key={index} variant={v}>
            MUI Button {v} variant
          </NewButton>
          <br />
          <br />
        </>
      ))}
    </div>
  );
};

export const MuiButtonColors = () => {
  const colors = [
    'inherit',
    'primary',
    'secondary',
    'success',
    'error',
    'info',
    'warning',
  ] as const;

  return (
    <>
      {colors.map((c, index) => (
        <NewButton key={index} color={c}>
          MUI Button {c}
        </NewButton>
      ))}
    </>
  );
};

export const MuiButtonSizes = () => {
  const sizes = ['small', 'medium', 'large'] as const;

  return (
    <div className="vertical">
      {sizes.map((s, index) => (
        <>
          <NewButton key={index} variant="outlined" size={s}>
            {s} MUI Button
          </NewButton>
          <br />
          <br />
        </>
      ))}
    </div>
  );
};

export const MuiButtonDisabled = () => (
  <NewButton variant="contained" disabled>
    Disabled :(
  </NewButton>
);
