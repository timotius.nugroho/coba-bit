import React from 'react';
import {
  Button as MuiButton,
  ButtonProps as MuiButtonProps,
} from '@mui/material';

export type NewButtonProps = { message?: string } & MuiButtonProps;

export const NewButton = ({ children, className, ...rest }: NewButtonProps) => {
  return (
    <MuiButton classes={className} {...rest}>
      {children}
    </MuiButton>
  );
};
