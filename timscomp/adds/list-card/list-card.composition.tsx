import React from 'react';
import { ListCard } from './list-card';

export const BasicListCard = () => {
  return <ListCard />;
};

export const ManyListCard = () => {
  return <ListCard numOfCard={15} />;
};
