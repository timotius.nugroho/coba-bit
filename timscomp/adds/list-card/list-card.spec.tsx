import React from 'react';
import { render } from '@testing-library/react';
import { BasicListCard } from './list-card.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicListCard />);
  const rendered = getByText('ok 1');
  expect(rendered).toBeTruthy();
});
