import React, { ReactNode } from 'react';
import { Card } from '@arunaseafood/timscomp.main.card';

export type ListCardProps = {
  /**
   * a node to be rendered in the special component.
   */
  numOfCard?: number;
};

export function ListCard({ numOfCard = 2 }: ListCardProps) {
  return (
    <div style={{ display: 'flex', gap: '8px' }}>
      {[...Array(numOfCard).keys()].map((e) => (
        <Card key={e} title={'ok ' + e} desc="lorem ipsum" />
      ))}
    </div>
  );
}
