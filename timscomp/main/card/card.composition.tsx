import React from 'react';
import { Card } from './card';

export const BasicCard = () => {
  return <Card title="OK" desc="lorem ipsum" />;
};

export const BlueCard = () => {
  return (
    <Card title="OK" desc="lorem ipsum" bgColor="blue" textColor="white" />
  );
};
