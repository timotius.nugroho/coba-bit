import React, { ReactNode } from 'react';
import classes from './card.module.scss';

export type CardProps = {
  /**
   * a node to be rendered in the special component.
   */
  title: string;
  desc: string;
  bgColor?: string;
  textColor?: string;
};

export function Card({
  title,
  desc,
  bgColor = 'white',
  textColor = 'black',
}: CardProps) {
  return (
    <div
      className={classes.card}
      style={{
        backgroundColor: bgColor,
        color: textColor,
      }}
    >
      <h3>{title}</h3>
      <p>{desc}</p>
    </div>
  );
}
