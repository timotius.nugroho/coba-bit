import React from 'react';
import { Button } from './button';

export const BasicButton = () => {
  return <Button>hello world!</Button>;
};

export const BlueButton = () => {
  return <Button bgColor={'blue'}>hello world!</Button>;
};
