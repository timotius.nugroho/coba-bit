import React, { ReactNode } from 'react';

export type ButtonProps = {
  /**
   * a node to be rendered in the special component.
   */
  children?: ReactNode;
  bgColor?: String;
};

export function Button({ children, bgColor = 'red' }: ButtonProps) {
  return (
    <div
      style={{
        backgroundColor: bgColor as any,
        padding: '10px',
        borderRadius: '15px',
      }}
    >
      {children}
    </div>
  );
}
