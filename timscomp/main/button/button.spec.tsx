import React from 'react';
import { render } from '@testing-library/react';
import { Button } from '@arunaseafood/timscomp.main.button';

it('should render with the correct text', () => {
  const { getByText } = render(
    <>
      <Button>Hallo</Button>
    </>
  );
  const rendered = getByText('Hallo');
  expect(rendered).toBeTruthy();
});
