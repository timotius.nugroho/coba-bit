export const defaultTheme = {
  palette: {
    primary: {
      main: '#fc8403',
    },
    secondary: {
      main: '#30b353',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          padding: '15px 36px',
          textTransform: 'none',
          '&.Mui-disabled': {
            border: '1px solid black',
          },
        },
        containedPrimary: {
          backgroundColor: '#fc8403',
          '&:hover': {
            color: 'red',
          },
          '&:active': {
            color: 'black',
          },
        },
      },
    },
    MuiSkeleton: {
      styleOverrides: {
        root: {
          backgroundColor: '#fc8403',
        },
      },
    },
  },
};
